from django.db import models
from django.urls import reverse


class Salesperson(models.Model):
    first_name = models.CharField(max_length=100, null=True)
    last_name = models.CharField(max_length=100, null=True)
    employee_id = models.PositiveIntegerField(unique=True)

    def get_api_url(self):
        return reverse("sales_person_delete", kwargs={"id": self.pk})

    def __str__(self):
        return f"{self.employee_id}: {self.first_name} {self.last_name}"


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class Customer(models.Model):
    first_name = models.CharField(max_length=100, null=True)
    last_name = models.CharField(max_length=100, null=True)
    address = models.TextField()
    phone_number = models.PositiveBigIntegerField()

    def get_api_url(self):
        return reverse("customer_delete", kwargs={"id": self.pk})

    def __str__(self):
        return f"{self.id} {self.first_name} {self.last_name}"


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sale",
        on_delete=models.CASCADE,
    )
    sales_person = models.ForeignKey(
        Salesperson,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    price = models.PositiveBigIntegerField()

    def get_api_url(self):
        return reverse("sales_delete", kwargs={"id": self.pk})
