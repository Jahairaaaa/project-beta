import { NavLink } from 'react-router-dom';


function Nav() {
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-dark">
        <div style={{backgroundColor: "rgba(0, 238, 130, .8)"}} className="container-fluid">
          <NavLink style={{paddingRight: "30px", color: "black"}} className="navbar-brand" to="/">CarCar</NavLink>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li style={{paddingRight: "40px"}} className="nav-item">
                <NavLink className="nav-link" to="/manufacturers/">Manufacturers</NavLink>
              </li>
              <li style={{paddingRight: "40px"}} className="nav-item">
                <NavLink className="nav-link" to="/manufacturers/new/">Create a Manufacturer</NavLink>
              </li>
              <li style={{paddingRight: "40px"}} className="nav-item">
                <NavLink className="nav-link" to="/models/">Models</NavLink>
              </li>
              <li style={{paddingRight: "40px"}} className="nav-item">
                <NavLink className="nav-link" to="/models/new/">Create a Model</NavLink>
              </li>
              <li style={{paddingRight: "40px"}} className="nav-item">
                <NavLink className="nav-link" to="/automobiles/">Automobiles</NavLink>
              </li>
              <li style={{paddingRight: "40px"}} className="nav-item">
                <NavLink className="nav-link" to="/automobiles/new/">Create an Automobile</NavLink>
              </li>
              <li style={{paddingRight: "40px"}} className="nav-item">
                <NavLink className="nav-link" to="/salespeople/">Salespeople</NavLink>
              </li>
              <li style={{paddingRight: "40px"}} className="nav-item">
                <NavLink className="nav-link" to="/salespeople/new/">Add a Salesperson</NavLink>
              </li>
              <li style={{paddingRight: "40px"}} className="nav-item">
                <NavLink className="nav-link" to="/customers/">Customers</NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <nav className="navbar navbar-expand-lg navbar-dark">
        <div style={{backgroundColor: "rgba(130, 238, 130, .8)"}} className="container-fluid">
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li style={{paddingRight: "50px"}} className="nav-item">
                <NavLink className="nav-link" to="/customers/new/">Create a Customer</NavLink>
              </li>
              <li style={{paddingRight: "50px"}} className="nav-item">
                <NavLink className="nav-link" to="/sales/">Sales</NavLink>
              </li>
              <li style={{paddingRight: "50px"}} className="nav-item">
                <NavLink className="nav-link" to="/sales/new/">Add a Sale</NavLink>
              </li>
              <li style={{paddingRight: "50px"}} className="nav-item">
                <NavLink className="nav-link" to="/sales/history/">Sales History</NavLink>
              </li>
              <li style={{paddingRight: "50px"}} className="nav-item">
                <NavLink className="nav-link" to="/technicians/">Technicians</NavLink>
              </li>
              <li style={{paddingRight: "50px"}} className="nav-item">
                <NavLink className="nav-link" to="/technicians/new/">Add a Technician</NavLink>
              </li>
              <li style={{paddingRight: "50px"}} className="nav-item">
                <NavLink className="nav-link" to="/services/">Service Appointments</NavLink>
              </li>
              <li style={{paddingRight: "50px"}} className="nav-item">
                <NavLink className="nav-link" to="/services/new/">Create a Service Appointment</NavLink>
              </li>
              <li style={{paddingRight: "50px"}} className="nav-item">
                <NavLink className="nav-link" to="/services/history/">Service History</NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  )
}

export default Nav;
