function MainPage() {
  return (
    <div style={{backgroundColor: "rgba(210, 238, 130, .5)"}} className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          <b>
            The premiere solution for automobile dealership
            management!
          </b>
        </p>
      </div>
    </div>
  );
}

export default MainPage;
