from django.db import models
from django.urls import reverse


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200, unique=True)

    def get_api_url(self):
        return reverse("api_technician", kwargs={"technicianId": self.pk})

    def __str__(self):
        return f"{self.employee_id}: {self.first_name} {self.last_name}"


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(default="created", max_length=200)
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=200)
    vip = models.BooleanField()
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.PROTECT,
        null=True,
    )

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"appointmentId": self.pk})


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin
